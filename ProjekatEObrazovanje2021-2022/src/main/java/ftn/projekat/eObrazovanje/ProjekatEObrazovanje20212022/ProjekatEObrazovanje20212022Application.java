	package ftn.projekat.eObrazovanje.ProjekatEObrazovanje20212022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class ProjekatEObrazovanje20212022Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjekatEObrazovanje20212022Application.class, args);
	}

}
